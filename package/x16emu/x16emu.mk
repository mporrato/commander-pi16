################################################################################
#
# x16emu
#
################################################################################

X16EMU_VERSION = master
X16EMU_SITE = $(call github,commanderx16,x16-emulator,$(X16EMU_VERSION))
X16EMU_LICENSE = BSD-2-Clause
X16EMU_LICENSE_FILES = LICENSE

X16EMU_DEPENDENCIES = sdl2

X16EMU_CFLAGS = -std=c99 -O3 -g0
X16EMU_CFLAGS += -I$(@D)/src/extern/include -I$(@D)/src/extern/src
X16EMU_CFLAGS += `$(STAGING_DIR)/usr/bin/sdl2-config --cflags`

define X16EMU_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) \
		CC="$(TARGET_CC)" \
		CFLAGS="$(TARGET_CFLAGS) $(X16EMU_CFLAGS)" -C $(@D)
endef

define X16EMU_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) cp "$(@D)/x16emu" "$(TARGET_DIR)/bin/"
endef

$(eval $(generic-package))
