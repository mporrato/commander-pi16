#!/bin/sh

set -u
set -e

# Run x16emu on boot
if [ -e ${TARGET_DIR}/etc/inittab ]; then
    grep -qE '^tty7::' ${TARGET_DIR}/etc/inittab || \
	sed -i '/HDMI console/a\
tty7::respawn:/bin/start.sh # run x16emu' ${TARGET_DIR}/etc/inittab
fi

configtxt="${BINARIES_DIR}/rpi-firmware/config.txt"

for arg in "$@";
do
    case "${arg}" in
        --enable-audio)
            # Enable analog audio
            if ! grep -qE '^dtparam=audio=' "${configtxt}"; then
                echo "Adding 'dtparam=audio=on' to config.txt."
                echo "dtparam=audio=on" >> "${configtxt}"
            fi
            ;;
        --overlay=*)
            # Add overlays
            ovlline="dtoverlay=${arg:10}"
            if ! grep -qE '^${ovlline}' "${configtxt}"; then
                echo "Adding '${ovlline}' to config.txt."
                echo "${ovlline}" >> "${configtxt}"
            fi
            ;;
    esac
done
