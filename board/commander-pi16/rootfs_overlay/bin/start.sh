#!/bin/sh

fatmnt=/mnt
x16emu=/bin/x16emu
x16emuargs=""
rootlabel=rootfs

workdir="${fatmnt}/x16"
romimage="${fatmnt}/rom.bin"
nvramimage="${fatmnt}/nvram.bin"


if ! mountpoint -q "${fatmnt}" ; then
    rootdev=$(realpath /dev/disk/by-label/${rootlabel})
    fatdev="${rootdev%2}1"
    [ -d "${fatmnt}" ] || mkdir -p "${fatmnt}"
    mount "${fatdev}" "${fatmnt}" || exit 1
fi

if [ -f "${romimage}" ] ; then
    mkdir -p "${workdir}"
    (
        cd "${workdir}"
        "${x16emu}" -rom "${romimage}" -nvram "${nvramimage}" ${x16emuargs}
    )
else
    echo "Can't find rom.bin in the root directory of the sdcard."
    while true ; do
        sleep 60
    done
fi
