# Commander Pi16

This is a collection of buildroot scripts and configuration files that allow
you to create your own [Commander X16](https://www.commanderx16.com/)
emulation box based on the Raspberry Pi.

What you will get **is not a baremetal emulator**: it is not a full Linux
distribution either but it still runs on top of the Linux kernel.

The boot time is about 25s on a Raspberry Pi 4 from the firmware splashscreen
to the CX16 ready prompt.

This is by no means a finished product: there is still a lot of work to do
in order to strip down the image to the bare minimum and to improve
performance, boot time and user experience.


## Building the bootable image

On the first line below, you might want to use the mirror at
`https://github.com/buildroot/buildroot` instead of the official git repo at
busybox.net .

```
git clone https://git.busybox.net/buildroot
git clone https://gitlab.com/mporrato/commander-pi16.git
cd buildroot
git checkout 2021.02.3 # Check out the latest stable release
# In the next command you need to pick the defconfig for your rpi version
# from the table in the next section.
make BR2_EXTERNAL=../commander-pi16 cpi16_raspberrypi_defconfig
make menuconfig # Only if you want to customise your build
make
```

This will take a while, especially the first time. Once the build is completed
you will find a file called `sdcard.img` in `output/images`. Just flash it to
an sd card using your favourite method.

Before booting the rpi you will have to drop a ROM image compatible with the
current emulator version in the root of the FAT32 partition. The file must be
named `rom.bin`.


## Supported boards

| Board                    | defconfig file                         |
|--------------------------|----------------------------------------|
| Raspberry Pi B+          | cpi16_raspberrypi_defconfig            |
| Raspberry Pi4 B          | cpi16_raspberrypi4_64_defconfig        |

Note: the Raspberry Pi B+ port is very slow, to the point of being barely
usable.
